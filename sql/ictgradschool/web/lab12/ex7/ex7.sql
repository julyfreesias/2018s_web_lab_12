DROP TABLE IF EXISTS player;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS soccerLeague;


CREATE TABLE IF NOT EXISTS soccerLeague(
  leaguename VARCHAR(50),
  description VARCHAR(50),
  PRIMARY KEY(leaguename)
);

CREATE TABLE IF NOT EXISTS team (
  t_id INT (10) NOT NULL,
  t_name VARCHAR(50) NOT NULL,
  city VARCHAR  (20),
  captain VARCHAR (50),
  score INT (100),
  leaguename VARCHAR(50),
  PRIMARY KEY(t_id),
  FOREIGN KEY(leaguename) REFERENCES  soccerLeague (leaguename)
);


CREATE TABLE IF NOT EXISTS player (

  p_id INT(5) NOT NULL ,
  name VARCHAR(50) NOT NULL,
  city VARCHAR  (20),
  captain VARCHAR (50),
  score INT (100),
  age INT (5),
  t_id INT (10),
  PRIMARY KEY(p_id),
  FOREIGN KEY(t_id) REFERENCES team (t_id)
);

INSERT INTO soccerLeague VALUES  ('melon', 'melon');

INSERT INTO team VALUES  ('8979','bulls','auckland','Andrew','365','melon');

INSERT INTO player VALUES  ('1','Jack Peter','auckland','John','65','65','8979');
INSERT INTO player VALUES  ('2','Nick Taka','auckland','John','200','65','8979');
INSERT INTO player VALUES  ('3','David Nick','hamilon','John','456','65','8979');
INSERT INTO player VALUES  ('4','Peterson David','auckland','John','265','65','8979');
INSERT INTO player VALUES  ('5','Peter auckland','nelson','John','365','65','8979');
INSERT INTO player VALUES  ('6','John Taka','auckland','John','565','65','8979');
INSERT INTO player VALUES  ('7','Taka Andrew','auckland','John','365','65','8979');
INSERT INTO player VALUES  ('8','Andrew Tom','auckland','John','665','65','8979');
INSERT INTO player VALUES  ('9','Josh Andrew','auckland','John','365','65','8979');



