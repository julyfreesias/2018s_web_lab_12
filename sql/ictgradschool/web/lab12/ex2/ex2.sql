-- Answers to exercise 2 questions
SELECT s.fname,s.lname
FROM unidb_students AS s, unidb_attend AS a
WHERE s.id = a.id AND a.dept = 'comp' AND a.num = 219;

SELECT s.fname,s.lname
FROM unidb_students AS s, unidb_attend AS a
WHERE s.id = a.id AND s.country <> 'NZ'

SELECT office
FROM unidb_lecturers AS s, unidb_teach AS a
WHERE s.staff_no = a.staff_no AND a.num = 219;

SELECT DISTINCT s.fname,s.lname
FROM unidb_lecturers AS l, unidb_teach AS t,
     unidb_students AS s, unidb_attend AS a
WHERE l.staff_no = t.staff_no AND
      a.id =s.id AND  a.dept=t.dept AND a.num=t.num
      AND l.fname='Te Taka'


SELECT s1.fname AS StudetFirstName, s1.lname AS StudentLastName, s2.fname AS men
FROM unidb_students s1, unidb_students s2
WHERE s1.mentor = s2. id

SELECT l.fname, l.lname
FROM unidb_lecturers AS l
WHERE office ='G%'
UNION
SELECT s.fname, s.lname
FROM unidb_students AS s
WHERE country <> 'NZ'


SELECT l.fname,l.lname
FROM unidb_courses AS c,unidb_lecturers AS l
WHERE c.coord_no = l.staff_no AND dept='comp' AND num='219'
UNION
SELECT s.fname,s.lname
FROM unidb_students AS s,unidb_courses AS c
WHERE s.id = c.rep_id AND dept='comp' AND num='219'