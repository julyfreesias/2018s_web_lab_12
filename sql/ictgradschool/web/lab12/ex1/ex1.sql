-- Answers to exercise 1 questions
SELECT DISTINCT dept
FROM unidb_courses

SELECT DISTINCT semester
FROM unidb_attend

SELECT DISTINCT dept, num
FROM unidb_attend

SELECT fname,country
FROM unidb_students
ORDER BY fname

SELECT fname,mentor
FROM unidb_students
ORDER BY mentor

SELECT *
FROM unidb_lecturers
ORDER BY office

SELECT *
FROM unidb_lecturers
WHERE staff_no>500

SELECT *
FROM unidb_students
WHERE id BETWEEN 1668 AND 1824

SELECT *
FROM unidb_students
WHERE  country = 'NZ'
OR country = 'AU'
OR country = 'US'

SELECT *
FROM unidb_lecturers
WHERE office LIKE 'G%'

SELECT *
FROM unidb_courses
WHERE dept <> 'comp'  -- WHERE NOT dept = 'comp' is the same.

SELECT *
FROM unidb_students
WHERE  country = 'FR'
OR country = 'MX'


